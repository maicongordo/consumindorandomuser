﻿using ConsumindoRandomUser.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoRandomUser
{
    class Program
    {


        static void Main(string[] args)
        {
            Console.WriteLine("Digite o número para a pesquisa: ");
            string resp = Console.ReadLine();
            Console.Write("\n");



            string url = "https://randomuser.me/api/?results=" + resp ;

            RandomUsers randomUsers = BuscarUsuarios (url);

           
            foreach (Result result in randomUsers.Results)
            {
                Console.Write("\n RESULTS:\n");
                Console.WriteLine(string.Format("Gender: {0}  \nTitle: {1} \nFirst: {2} \nLast: {3}",
                    result.Gender,result.Name.Title,result.Name.First,result.Name.Last ));

                Console.Write("\n LOCATION:\n");
                Console.WriteLine(string.Format("Number: {0}  \nName: {1} \n\nCity: {2} \nState: {3} \nCountry: {4} \nPostcode: {5}   ",
                    result.Location.Street.Number, result.Location.Street.Name, result.Location.City, result.Location.State, result.Location.Country, result.Location.Postcode));

                Console.Write("\n COORDINATES:\n");
                Console.WriteLine(string.Format("Latitude: {0}  \nLongitude: {1} ",
                    result.Location.Coordinates.latitude, result.Location.Coordinates.longitude));

                Console.Write("\n TIMEZONE:\n");
                Console.WriteLine(string.Format("Offset: {0}  \nDescription: {1}  ",
                    result.Location.Timezone.offset, result.Location.Timezone.description));

                Console.Write("\n EMAIL:\n");
                Console.WriteLine(result.Email);
                Console.Write("\n ------------------------------------------------------------------------------------------------- \n");

            }


            


            Console.ReadLine();
        }



        public static RandomUsers BuscarUsuarios (string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.Write("\n \n");

            RandomUsers randomUsers = JsonConvert.DeserializeObject<RandomUsers>(content);
            return randomUsers;

        }



    }
}
