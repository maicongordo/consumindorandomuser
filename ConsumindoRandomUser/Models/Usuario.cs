﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsumindoRandomUser.Models
{
    class RandomUsers
    {
        public List<Result> Results { get; set; }
    }

    class Result
    {
        public string Gender { get; set; }
        public string Email { get; set; }
        public Name Name  { get; set; }
        public Location Location { get; set; }
    }
    

    class Name
    {
        public string Title { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
    }

    public class Street
    {
        public int Number { get; set; }
        public string Name { get; set; }
    }

    public class Location
    {
        public Street Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Postcode { get; set; }
        public Coordinates Coordinates { get; set; }
        public Timezone Timezone { get; set; }
    }

    public class Coordinates
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class Timezone
    {
        public string offset { get; set; }
        public string description { get; set; }
    }
    
}
